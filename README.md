## TestMotor: A System for Tree-Based Test Data Generation for Programming Assignments

TestMotor is a program for automatically generating arguments for Java methods and constructors. Generated arguments
can be used as test data in unit test creation. TestMotor is designed to be used for programming assignments tests, as
it offers options for specifying the difficulty of generated test data and functionality that should be tested.

Download JAR file: [testmotor-1.1.jar](https://bitbucket.org/ENigola/testmotor/downloads/testmotor-1.1.jar)

### Running from command line

    java -jar testmotor.jar <fully qualified method or class (for constructor) name> [-min_height int]
                            			[-max_height int] [-seed int] [-static_imports boolean] [-allow_exceptions boolean] [-display_values boolean]

Example:

    java -jar testmotor.jar java.util.Date.after -max_height 4 -seed 42 -allow_exceptions false

If generating test data for methods of own classes, then their bytecode needs to be added to classpath.
In that case use command:

    java -cp "path_to_testmotor_jar\testmotor.jar;path_to_own_compiled_classes" testmotor.TestMotorRunner my.package.MyClass.myMethod [other arguments]

### Using TestMotor from code

Firstly add testmotor.jar as dependancie or to classpath when running.

Example usage:

```java
// create new TestMotor instance
TestMotor tm = new TestMotor();
// set seed for randomness
tm.setSeed(42);
Method method;
try {
    // find target method
    method = java.util.Date.class.getMethod("after", Date.class);
} catch (NoSuchMethodException e) {
    throw new RuntimeException(e);
}
int height = 3; // generated tree (test data) height
boolean allowExceptions = false; // generated test data won't throw exceptions
try {
    // generate single instance of test data
    ValueNode testData = tm.generateTestData(method, height, null, allowExceptions);
    // display generated test data as a Java expression
    System.out.println(testData.stringify());
} catch (GenerationException e) {
    System.out.println("Test data generation failed!");
}
```

Example output:

    new java.util.Date().after(java.util.Date.from(java.time.Instant.ofEpochMilli(-292L)))
    
If test data need to be generate for sets of allowed executables (methods and constructors) and for different heights, then the convenience method _generateTestDataCombinatorial_ can be used:

```java
// create TestMotor instance
TestMotor tm = new TestMotor();
// get references to executables
Executable targetExecutable = StringBuilder.class.getMethod("append", int.class);
Executable allowedExecutable1 = StringBuilder.class.getMethod("append", String.class);
Executable allowedExecutable2 = StringBuilder.class.getMethod("reverse");
Executable allowedExecutable3 = StringBuilder.class.getMethod("insert", int.class, String.class);
Executable allowedExecutable4 = StringBuilder.class.getConstructor(String.class);
// first set of allowed executables
Set<Executable> allowedExecutables1 = new HashSet<>();
allowedExecutables1.add(allowedExecutable1);
allowedExecutables1.add(allowedExecutable4);
// second set of allowed executables
Set<Executable> allowedExecutables2 = new HashSet<>();
allowedExecutables2.add(allowedExecutable2);
allowedExecutables2.add(allowedExecutable3);
allowedExecutables2.add(allowedExecutable4);
// list of all sets of allowed executables
List<Set<Executable>> setsOfAllowedExecutables = new ArrayList<>();
setsOfAllowedExecutables.add(allowedExecutables1);
setsOfAllowedExecutables.add(allowedExecutables2);
// list of heights
List<Integer> heights = new ArrayList<>();
heights.add(3);
heights.add(5);
// generate test data
List<List<ValueNode>> testData = tm.generateTestDataCombinatorial(targetExecutable, heights, setsOfAllowedExecutables, 3, true);
for (List<ValueNode> testDataPerSetOfExecutables : testData) {
    for (ValueNode testDataInstance : testDataPerSetOfExecutables) {
        System.out.println(testDataInstance.stringify());
    }
    System.out.println("-------");
}
```

Example output:

    new java.lang.StringBuilder("9vwxomp8").append("Xgc").append(273)
    new java.lang.StringBuilder("9k").append("8").append(59)
    new java.lang.StringBuilder("3mjP").append("").append(-61)
    new java.lang.StringBuilder("kgxcmma3").append("3").append("Wv").append("V").append(-50)
    new java.lang.StringBuilder("qb").append("2").append("f").append("vKohqv").append(237)
    new java.lang.StringBuilder("6R2N7b").append("m").append("l").append("j").append(318)
    -------
    new java.lang.StringBuilder("ks").insert(64, "ifluIsw").append(120)
    new java.lang.StringBuilder("A").insert(-33, "7PfEx").append(-119)
    new java.lang.StringBuilder("c6d").reverse().append(252)
    new java.lang.StringBuilder("b").insert(113, "hkR2ZZ").insert(-39, "h").reverse().append(-113)
    new java.lang.StringBuilder("yIx2").reverse().reverse().insert(221, "v").append(134)
    new java.lang.StringBuilder("o").insert(330, "q").insert(279, "cifNnd").reverse().append(-46)
    -------
