package classes.math;

public class MathNumber extends MathExpression {

    private int value;

    public MathNumber(int value) {
        this.value = value;
    }

    @Override
    public int evaluate() {
        return value;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MathNumber) {
            return ((MathNumber) obj).value == value;
        }
        return false;
    }


}
