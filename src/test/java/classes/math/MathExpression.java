package classes.math;

public abstract class MathExpression {

    public abstract int evaluate();

    @Override
    public abstract boolean equals(Object obj);
}
