package classes.math;

public class MathAdd extends MathBinaryOp {

    public MathAdd(MathExpression left, MathExpression right) {
        super(MathOpSign.ADD, left, right);
    }

    @Override
    public int evaluate() {
        return left.evaluate() + right.evaluate();
    }
}
