package classes.math;

import java.util.List;

public class MathUtils {

    public static int arraySum(MathExpression[] exps) {
        int sum = 0;
        for (MathExpression exp : exps) {
            sum += exp.evaluate();
        }
        return sum;
    }

    public static int varargsSum(MathExpression... exps) {
        int sum = 0;
        for (MathExpression exp : exps) {
            sum += exp.evaluate();
        }
        return sum;
    }

    public static int listSum(List<MathExpression> exps) {
        int sum = 0;
        for (MathExpression exp : exps) {
            sum += exp.evaluate();
        }
        return sum;
    }

    public static void assertIsAddOperation(MathExpression exp) {
        if (!(exp instanceof MathAdd)) {
            throw new RuntimeException("exp is not add operation");
        }
    }
}
