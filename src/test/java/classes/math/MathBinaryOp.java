package classes.math;

public abstract class MathBinaryOp extends MathExpression {

    public enum MathOpSign {ADD, SUBTRACT}

    private MathOpSign operation;
    protected MathExpression left;
    protected MathExpression right;

    protected MathBinaryOp(MathOpSign operation, MathExpression left, MathExpression right) {
        this.operation = operation;
        this.left = left;
        this.right = right;
    }

    public static MathBinaryOp create(MathOpSign operation, MathExpression left, MathExpression right) {
        switch (operation) {
            case ADD:
                return new MathAdd(left, right);
            case SUBTRACT:
                return new MathSubtract(left, right);
            default:
                throw new RuntimeException("Unknown math operation");
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MathBinaryOp) {
            MathBinaryOp binaryOp = (MathBinaryOp) obj;
            return binaryOp.operation.equals(operation)
                    && binaryOp.left.equals(left)
                    && binaryOp.right.equals(right);
        }
        return false;
    }
}
