package classes.math;

public class MathSubtract extends MathBinaryOp {

    public MathSubtract(MathExpression left, MathExpression right) {
        super(MathOpSign.SUBTRACT, left, right);
    }

    @Override
    public int evaluate() {
        return left.evaluate() + right.evaluate();
    }
}
