package testmotor;

import classes.math.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import testmotor.tree.ValueNode;

import java.lang.reflect.Constructor;
import java.lang.reflect.Executable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class TestMotorTest {

    private static int seed = 42;
    private static TestMotor tm;

    private static Constructor mathNumber;
    private static Constructor mathAdd;
    private static Constructor mathSubtract;
    private static Method mathEvaluate;
    private static Method binaryOpCreate;
    private static Method arraySum;
    private static Method listSum;
    private static Method assertIsAddOp;


    static {
        try {
            mathNumber = MathNumber.class.getConstructor(int.class);
            mathAdd = MathAdd.class.getConstructor(MathExpression.class, MathExpression.class);
            mathSubtract = MathSubtract.class.getConstructor(MathExpression.class, MathExpression.class);
            mathEvaluate = MathExpression.class.getMethod("evaluate");
            binaryOpCreate = MathBinaryOp.class.getMethod("create", MathBinaryOp.MathOpSign.class, MathExpression.class, MathExpression.class);
            arraySum = MathUtils.class.getMethod("arraySum", MathExpression[].class);
            listSum = MathUtils.class.getMethod("listSum", List.class);
            assertIsAddOp = MathUtils.class.getMethod("assertIsAddOperation", MathExpression.class);
        } catch (NoSuchMethodException e) {
            // ignore
        }
    }

    @BeforeAll
    static void init() {
        tm = new TestMotor();
        tm.setSeed(seed);
    }

    @Test
    void testBasicTestDataGeneration() throws GenerationException {

        ValueNode result1 = tm.generateTestData(mathEvaluate, 3, null, true);
        ValueNode result2 = tm.generateTestData(mathEvaluate, 4, null, true);
        ValueNode result3 = tm.generateTestData(mathEvaluate, 5, null, true);
        assertTrue(result1.stringify().contains("evaluate("));
        assertTrue(result2.stringify().contains("evaluate("));
        assertTrue(result3.stringify().contains("evaluate("));

        ValueNode result4 = tm.generateTestData(arraySum, 3, null, true);
        ValueNode result5 = tm.generateTestData(arraySum, 4, null, true);
        ValueNode result6 = tm.generateTestData(arraySum, 5, null, true);
        assertTrue(result4.stringify().contains("arraySum("));
        assertTrue(result5.stringify().contains("arraySum("));
        assertTrue(result6.stringify().contains("arraySum("));

        ValueNode result7 = tm.generateTestData(mathAdd, 10, null, true);
        ValueNode result8 = tm.generateTestData(mathAdd, 15, null, true);
        ValueNode result9 = tm.generateTestData(mathAdd, 20, null, true);
        assertTrue(result7.stringify().contains("new classes.math.MathAdd("));
        assertTrue(result8.stringify().contains("new classes.math.MathAdd("));
        assertTrue(result9.stringify().contains("new classes.math.MathAdd("));
    }

    @Test
    void testImpossibleGeneration() throws GenerationException {
        tm.generateTestData(mathAdd, 2, null, true);
        assertThrows(GenerationException.class, () -> tm.generateTestData(mathAdd, 1, null, true));
        tm.generateTestData(arraySum, 3, null, true);
        tm.generateTestData(arraySum, 1, null, true); // empty array
        assertThrows(GenerationException.class, () -> tm.generateTestData(arraySum, 2, null, true));
    }

    @Test
    void testAllowedExecutables() throws GenerationException {
        Set<Executable> allowedExecutables = new HashSet<>();
        allowedExecutables.add(mathAdd);
        allowedExecutables.add(mathNumber);
        for (int i = 0; i < 5; i++) {
            ValueNode result = tm.generateTestData(mathAdd, 5, allowedExecutables, true);
            String resultStr = result.stringify();
            assertTrue(resultStr.contains("MathNumber"));
            assertTrue(resultStr.contains("MathAdd"));
            assertFalse(resultStr.contains("MathSubtract"));
            assertFalse(resultStr.contains("MathBinaryOp"));
        }
    }

    @Test
    void testExceptionsNotAllowed() throws GenerationException, InvocationTargetException {
        for (int i = 0; i < 10; i++) {
            tm.generateTestData(assertIsAddOp, 4,null, false).invoke();
        }
    }

    @Test
    void testCombinatorialTestDataGeneration() {
        Set<Executable> allowedExecutables1 = new HashSet<>();
        allowedExecutables1.add(mathNumber);
        allowedExecutables1.add(mathSubtract);
        Set<Executable> allowedExecutables2 = new HashSet<>();
        allowedExecutables2.add(mathNumber);
        allowedExecutables2.add(binaryOpCreate);
        List<Set<Executable>> allowedExecutablesList = Arrays.asList(allowedExecutables1, allowedExecutables2);
        List<Integer> heights = Arrays.asList(3,5,7);
        int amountPerCombination = 3;
        List<List<ValueNode>> results = tm.generateTestDataCombinatorial(mathEvaluate, heights, allowedExecutablesList, amountPerCombination, true);

        assertEquals(2, results.size());
        List<ValueNode> resultsForExecutables1 = results.get(0);
        List<ValueNode> resultsForExecutables2 = results.get(1);
        assertEquals(heights.size() * amountPerCombination, resultsForExecutables1.size());
        assertEquals(heights.size() * amountPerCombination, resultsForExecutables2.size());
        for (ValueNode testData : resultsForExecutables1) {
            String testDataStr = testData.stringify();
            assertTrue(testDataStr.contains("MathNumber") || testDataStr.contains("MathSubtract"));
            assertFalse(testDataStr.contains("MathAdd"));
            assertFalse(testDataStr.contains("MathBinaryOp"));
        }
        for (ValueNode testData : resultsForExecutables2) {
            String testDataStr = testData.stringify();
            assertTrue(testDataStr.contains("MathNumber") || testDataStr.contains("MathBinaryOp"));
            assertFalse(testDataStr.contains("MathAdd"));
            assertFalse(testDataStr.contains("MathSubtract"));
        }
    }

    @Test
    void testGenericCollectionsGeneration() throws GenerationException, InvocationTargetException {
        for (int i = 0; i < 10; i++) {
            ValueNode result = tm.generateTestData(listSum, 5, null, true);
            result.invoke(); // assert that no ClassCastException is thrown
        }
    }

}
