package testmotor.tree;

import classes.math.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class ExecNodeTest {

    private static ExecNode mathNumberNode; // constructor
    private static ExecNode mathAddNode; // constructor
    private static ExecNode mathBinaryCreateNode; // class method (static method)
    private static ExecNode mathEvaluateNode; // instance method
    private static ExecNode varargsSumNode; // varargs static method

    @BeforeAll
    static void init() throws NoSuchMethodException {
        Constructor mathNumberConstructor = MathNumber.class.getConstructor(int.class);
        mathNumberNode = new ExecNode(mathNumberConstructor, null, Collections.singletonList(new LeafNode(int.class, 7)));
        Constructor mathAddConstructor = MathAdd.class.getConstructor(MathExpression.class, MathExpression.class);
        mathAddNode = new ExecNode(mathAddConstructor, null, Arrays.asList(
                new ExecNode(mathNumberConstructor, null, Collections.singletonList(new LeafNode(int.class, 1))),
                new ExecNode(mathNumberConstructor, null, Collections.singletonList(new LeafNode(int.class, 2)))
        ));
        Method mathBinaryCreateMethod = MathBinaryOp.class.getMethod("create", MathBinaryOp.MathOpSign.class, MathExpression.class, MathExpression.class);
        mathBinaryCreateNode = new ExecNode(mathBinaryCreateMethod, null, Arrays.asList(
                new LeafNode(MathBinaryOp.MathOpSign.class, MathBinaryOp.MathOpSign.ADD),
                mathNumberNode,
                mathAddNode
        ));
        mathEvaluateNode = new ExecNode(MathExpression.class.getMethod("evaluate"), mathBinaryCreateNode, Collections.emptyList());
        varargsSumNode = new ExecNode(MathUtils.class.getMethod("varargsSum", MathExpression[].class), null, Collections.singletonList(
                new ArrayNode(MathExpression.class, Arrays.asList(mathNumberNode, mathAddNode))
        ));
    }

    @Test
    void testInstanceMethodWithNoParent() throws NoSuchMethodException {
        Method toString = Object.class.getMethod("toString");
        assertThrows(IllegalArgumentException.class, () -> new ExecNode(toString, null, new ArrayList<>()));
    }

    @Test
    void testInvoke() throws InvocationTargetException {
        MathExpression mathNumber = new MathNumber(7);
        MathExpression mathAdd = new MathAdd(new MathNumber(1), new MathNumber(2));
        MathExpression mathBinaryCreate = MathBinaryOp.create(MathBinaryOp.MathOpSign.ADD, mathNumber, mathAdd);
        int mathEvaluate = mathBinaryCreate.evaluate();
        int varargsSum = MathUtils.varargsSum(mathNumber, mathAdd);

        assertEquals(mathNumber, mathNumberNode.invoke());
        assertEquals(mathAdd, mathAddNode.invoke());
        assertEquals(mathBinaryCreate, mathBinaryCreateNode.invoke());
        assertEquals(mathEvaluate, mathEvaluateNode.invoke());
        assertEquals(varargsSum, varargsSumNode.invoke());
    }

    @Test
    void testStringify() {
        String mathNumberStr = "new classes.math.MathNumber(7)";
        String mathAddStr = "new classes.math.MathAdd(new classes.math.MathNumber(1), new classes.math.MathNumber(2))";
        String mathBinaryCreateStr = "classes.math.MathBinaryOp.create(classes.math.MathBinaryOp.MathOpSign.ADD, "
                + mathNumberStr + ", " + mathAddStr + ")";
        String mathEvaluateStr = mathBinaryCreateStr + ".evaluate()";
        String varargsSumStr = "classes.math.MathUtils.varargsSum(" + mathNumberStr + ", " + mathAddStr + ")";

        assertEquals(mathNumberStr, mathNumberNode.stringify());
        assertEquals(mathAddStr, mathAddNode.stringify());
        assertEquals(mathBinaryCreateStr, mathBinaryCreateNode.stringify());
        assertEquals(mathEvaluateStr, mathEvaluateNode.stringify());
        assertEquals(varargsSumStr, varargsSumNode.stringify());
    }
}
