package testmotor.tree;

import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ArrayNodeTest {

    private static ArrayNode emptyIntArray = new ArrayNode(int.class, Collections.emptyList());
    private static ArrayNode singletonIntArray = new ArrayNode(int.class, Collections.singletonList(new LeafNode(int.class, 5)));
    private static ArrayNode intArray = new ArrayNode(int.class, Arrays.asList(
            new LeafNode(int.class, 1),
            new LeafNode(int.class, 2),
            new LeafNode(int.class, 3),
            new LeafNode(int.class, 4),
            new LeafNode(int.class, 5))
    );
    private static ArrayNode stringArray = new ArrayNode(String.class, Arrays.asList(
            new LeafNode(String.class, "s1"),
            new LeafNode(String.class, "s2"),
            new LeafNode(String.class, "s3")
    ));

    @Test
    void testInvoke() throws InvocationTargetException {
        assertArrayEquals(new int[0], (int[]) emptyIntArray.invoke());
        assertArrayEquals(new int[] {5}, (int[]) singletonIntArray.invoke());
        assertArrayEquals(new int[] {1, 2, 3, 4, 5}, (int[]) intArray.invoke());
        assertArrayEquals(new String[] {"s1", "s2", "s3"}, (String[]) stringArray.invoke());
    }

    @Test
    void testStringify() {
        assertEquals("new int[] {}", emptyIntArray.stringify());
        assertEquals("new int[] {5}", singletonIntArray.stringify());
        assertEquals("new int[] {1, 2, 3, 4, 5}", intArray.stringify());
        assertEquals("new java.lang.String[] {\"s1\", \"s2\", \"s3\"}", stringArray.stringify());
    }
}
