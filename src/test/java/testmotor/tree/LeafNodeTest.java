package testmotor.tree;

import classes.math.MathAdd;
import classes.math.MathSubtract;
import org.junit.jupiter.api.Test;

import java.time.DayOfWeek;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class LeafNodeTest {

    private static LeafNode lnInt = new LeafNode(int.class, 7);
    private static LeafNode lnInteger = new LeafNode(Integer.class, 6);
    private static LeafNode lnDouble = new LeafNode(double.class, 8.5);
    private static LeafNode lnChar = new LeafNode(char.class, 'c');
    private static LeafNode lnBoolean = new LeafNode(boolean.class, true);
    private static LeafNode lnString = new LeafNode(String.class, "string");
    private static LeafNode lnEnum = new LeafNode(DayOfWeek.class, DayOfWeek.MONDAY);

    @Test
    void testNonMatchingTypes() {
        assertThrows(IllegalArgumentException.class, () -> new LeafNode(MathAdd.class, new MathSubtract(null, null)));
    }

    @Test
    void testInvoke() {
        assertEquals(7, lnInt.invoke());
        assertEquals(6, lnInteger.invoke());
        assertEquals(8.5, lnDouble.invoke());
        assertEquals('c', lnChar.invoke());
        assertEquals(true, lnBoolean.invoke());
        assertEquals("string", lnString.invoke());
        assertEquals(DayOfWeek.MONDAY, lnEnum.invoke());
    }

    @Test
    void testStringify() {
        assertEquals("7", lnInt.stringify());
        assertEquals("6", lnInteger.stringify());
        assertEquals("8.5", lnDouble.stringify());
        assertEquals("'c'", lnChar.stringify());
        assertEquals("true", lnBoolean.stringify());
        assertEquals("\"string\"", lnString.stringify());
        assertEquals("java.time.DayOfWeek.MONDAY", lnEnum.stringify());
    }

}
