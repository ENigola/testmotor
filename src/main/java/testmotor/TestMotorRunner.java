package testmotor;

import testmotor.tree.ValueNode;

import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.Executable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class for running TestMotor from command line, by providing the necessary information as program arguments. Provides
 * only limited access to TestMotor functionality. This class is mostly for testing and examples only.
 */
public class TestMotorRunner {
	private static final String usage = "Usage:\n" +
			"testmotor.jar <fully qualified method or class (for constructor) name> [-min_height int]"
			+ "[-max_height int] [-seed int] [-static_imports boolean] [-allow_exceptions boolean] [-display_values boolean]\n" +
			"Example arguments: java.util.Date.after -max_height 4 -seed 42 -allow_exceptions false";

	/**
	 * Creates TestMotor instance and generates and returns test data. 5 instances of test data will be generated for
	 * each height between minHeight and maxHeight (both inclusive).
	 * @param targetExecutable for which test data is generated
	 * @param seed RNG seed for TestMotor
	 * @param allowExceptions whether exception throwing test data is allowed
	 * @param minHeight lowest height to use for test data generation
	 * @param maxHeight highest height to use for test data generation
	 * @return generated test data, one list per each height with 5 test data instances
	 */
	private static List<List<ValueNode>> runTestMotor(Executable targetExecutable, Integer seed, boolean allowExceptions,
													 int minHeight, int maxHeight) {
		PrintStream log = System.out;
		int amountPerLevel = 5;
		TestMotor testMotor = new TestMotor();
		log.println("TestMotor instance created.");
		if (seed != null) {
			testMotor.setSeed(seed);
		}
		List<List<ValueNode>> allTestData = new ArrayList<>();
		for (int height = minHeight; height <= maxHeight; height++) {
			List<ValueNode> oneLevelTestData = new ArrayList<>();
			for (int i = 0; i < amountPerLevel; i++) {
				try {
					ValueNode testData = testMotor.generateTestData(targetExecutable, height, null, allowExceptions);
					oneLevelTestData.add(testData);
				} catch (GenerationException e) {
					// continue;
				}
			}
			if (oneLevelTestData.size() != amountPerLevel) {
				String failAmount = oneLevelTestData.isEmpty() ? "all" : "" + (amountPerLevel - oneLevelTestData.size());
				log.println("Generation for height " + height + " failed on " + failAmount + " instances.");
			}
			log.println("Generation for height " + height + " done.");
			allTestData.add(oneLevelTestData);
		}
		return allTestData;
	}

	/**
	 * Program's main method. Parses arguments, generates corresponding test data and displays it.
	 * <br>
	 * If multiple executables with the same name exist, then the one with the least arguments is chosen.
	 * Assumes that package and method names start with lowercase letters and class names with capital letters.
	 * @param args program arguments
	 */
	public static void main(String[] args) {
		PrintStream out = System.out;
		if (args.length == 0) {
			out.println(usage);
			return;
		}
		// Find target executable (method or constructor)
		String fullExecName = args[0];
		String[] nameParts = fullExecName.split("\\.");
		boolean isConstructor = false;
		String className;
		String methodName;
		if (Character.isLowerCase(nameParts[nameParts.length - 1].charAt(0))) {
			// method call
			className = fullExecName.split("\\.[^.]*$")[0];
			methodName = nameParts[nameParts.length - 1];
		} else {
			// constructor call
			isConstructor = true;
			className = fullExecName;
			methodName = null;
		}
		Class targetClass;
		try {
			targetClass = Class.forName(className);
		} catch (ClassNotFoundException e) {
			out.println("Class " + className + " not found!");
			return;
		}
		List<Executable> executables = new ArrayList<>();
		if (isConstructor) {
			executables = Arrays.asList(targetClass.getDeclaredConstructors());
		} else {
			Method[] methods = targetClass.getDeclaredMethods();
			for (Method m : methods) {
				if (m.getName().equals(methodName)) {
					executables.add(m);
				}
			}
		}
		Executable targetExecutable = null;
		int minArgumentCount = Integer.MAX_VALUE;
		for (Executable exec : executables) {
			if (exec.getParameterCount() < minArgumentCount) {
				minArgumentCount = exec.getParameterCount();
				targetExecutable = exec;
			}
		}
		if (targetExecutable == null) {
			out.println("Executable not found");
			return;
		} else {
			out.println("Target executable: " + targetExecutable);
		}
		// Parse other arguments
		Integer seed = null;
		int minHeight = 1;
		int maxHeight = 5;
		boolean staticImport = false;
		boolean allowExceptions = true;
		boolean displayValues = false;
		String errorMessage = null;
		for (int i = 1; i < args.length; i += 2) {
			String arg = args[i];
			String nextArg;
			if (i < args.length - 1){
				nextArg = args[i+1];
			} else {
				nextArg = "";
			}
			if (arg.equals("-static_imports")) {
				if (nextArg.equals("true")) {
					staticImport = true;
				} else if (nextArg.equals("false")) {
					staticImport = false;
				} else {
					errorMessage = "Invalid value for parameter static_import";
				}
			} else if (arg.equals("-seed")) {
				try {
					seed = Integer.valueOf(nextArg);
				} catch (NumberFormatException e) {
					errorMessage = "Invalid seed";
				}
			} else if (arg.equals("-allow_exceptions")) {
				if (nextArg.equals("true")) {
					allowExceptions = true;
				} else if (nextArg.equals("false")) {
					allowExceptions = false;
				} else {
					errorMessage = "Invalid value for parameter allow_exceptions";
				}
			} else if (arg.equals("-min_height")) {
				try {
					minHeight = Integer.valueOf(nextArg);
					if (minHeight < 0) {
						errorMessage = "min_height must be non-negative";
					}
				} catch (NumberFormatException e) {
					errorMessage = "Invalid min_height";
				}
			} else if (arg.equals("-max_height")) {
				try {
					maxHeight = Integer.valueOf(nextArg);
					if (maxHeight < 0) {
						errorMessage = "max_height must be non-negative";
					}
				} catch (NumberFormatException e) {
					errorMessage = "Invalid max_height";
				}
			} else if (arg.equals("-display_values")) {
				if (nextArg.equals("true")) {
					displayValues = true;
				} else if (nextArg.equals("false")) {
					displayValues = false;
				} else {
					errorMessage = "Invalid value for parameter display_values";
				}
			} else {
				errorMessage = "Unknown argument: " + arg;
			}
			if (errorMessage != null) {
				out.println(errorMessage);
				return;
			}
		}
		if (maxHeight < minHeight) {
			out.println("min_height must be smaller or equal to max_height");
			return;
		}
		out.println("Generating for heights " + minHeight + " to " + maxHeight);
		// Generate test data
		List<List<ValueNode>> testDataAll = runTestMotor(targetExecutable, seed, allowExceptions, minHeight, maxHeight);
		// Display results
		List<String> lines = new ArrayList<>();
		for (int i = 0; i < testDataAll.size(); i++) {
			lines.add("// height " + (i + 1));
			List<ValueNode> testDataOneLevel = testDataAll.get(i);
			for (ValueNode testData : testDataOneLevel) {
				String line = testData.stringify();
				if (displayValues) {
					line += " // == ";
					// disable System.out while executing
					PrintStream originalOut = System.out;
					PrintStream dummyOut = new PrintStream(new OutputStream() {
						@Override
						public void write(int b) {}
					});
					System.setOut(dummyOut);
					try {
						Object invokeResult = testData.invoke();
						if (invokeResult != null) {
							line += invokeResult.toString();
						} else {
							line += "null";
						}
					} catch (InvocationTargetException e) {
						line += e.getTargetException().getClass().getName() + ": " + e.getTargetException().getMessage();
					} finally {
						System.setOut(originalOut);
					}
				}
				lines.add(line);
			}
			lines.add("");
		}
		List<String> newLines = separateImports(lines, staticImport);
		out.println("Output:");
		out.println();
		for (String line : newLines) {
			out.println(line);
		}
	}

	/**
	 * Separates class and package names from fully qualified names and makes import statements for them. Takes a list of
	 * strings representing Java expressions and returns a list of strings, starting with import statements and ending
	 * with the same expressions, but with simplified names.
	 * @param lines list of Java expressions from which to separate imports
	 * @param staticImport whether static imports should be used for static methods
	 * @return list of import statements and expressions with simplified names
	 */
	public static List<String> separateImports(List<String> lines, boolean staticImport) {
		String identifier = "[a-zõäöüA-ZÕÄÖÜ_$][a-zõäöüA-ZÕÄÖÜ0-9_$]*";
		String callRegex = "((?:new )?(?:" + identifier + "\\.)+" + identifier + ")";
		Pattern pattern = Pattern.compile(callRegex);
		Set<String> imports = new HashSet<>();
		List<String> newLines = new ArrayList<>();
		for (String line : lines) {
			if (line.equals("") || line.startsWith("//")) {
				newLines.add(line);
				continue;
			}
			String[] halves = line.split("//");
			line = halves[0];
			String comment = null;
			if (halves.length == 2) {
				comment = halves[1];
			}
			Matcher matcher = pattern.matcher(line);
			StringBuffer sb = new StringBuffer();
			while (matcher.find()) {
				String call = matcher.group(1);
				String replacement;
				if (call.startsWith("new ")) {
					imports.add("import " + call.substring(4) + ";");
					replacement = "new " + call.substring(call.lastIndexOf('.') + 1);
				} else {
					String[] parts = call.split("\\.");
					if (staticImport) {
						imports.add("import static " + call + ";");
						replacement = parts[parts.length - 1];
					} else {
						int classEndIndex = call.lastIndexOf('.');
						if (classEndIndex == -1) {
							replacement = call;
						} else {
							imports.add("import " + call.substring(0, classEndIndex) + ";");
							replacement = parts[parts.length - 2] + "." + parts[parts.length - 1];
						}
					}
				}
				matcher.appendReplacement(sb, replacement);
			}
			matcher.appendTail(sb);
			if (comment != null) {
				sb.append("//").append(comment);
			}
			newLines.add(sb.toString());
		}
		newLines.add(0, "");
		// remove java.lang.* classes, because they don't require explicit import
		List<String> importsNew = new ArrayList<>();
		for (String importLine : imports) {
			if (!importLine.startsWith("import java.lang")) {
				importsNew.add(importLine);
			}
		}
		newLines.addAll(0, importsNew);
		return newLines;
	}
}
