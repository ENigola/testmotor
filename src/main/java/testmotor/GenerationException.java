package testmotor;

/**
 * An exception for test data generation process. Indicates that data generation was unsuccessful.
 */
public class GenerationException extends Exception {

	public GenerationException() { }

	public GenerationException(String message) {
		super(message);
	}
}
