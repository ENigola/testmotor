package testmotor;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


/**
 * A class containing static utility methods used by other classes of the project
 */
public final class Utils {

    private Utils() {}

    public static boolean isConstructor(Executable executable) {
        return executable instanceof Constructor;
    }

    public static boolean isMethod(Executable executable) {
        return executable instanceof Method;
    }

    public static boolean isInstanceMethod(Executable executable) {
        return isMethod(executable) && !Modifier.isStatic(executable.getModifiers());
    }

    /**
     * Generates a shuffled list of all integers from specified range
     * @param random source of randomness
     * @param start start of integer range (inclusive)
     * @param end end of integer range (exclusive)
     * @return list of integers from specified range in random order
     */
    public static List<Integer> shuffledRange(Random random, int start, int end) {
        List<Integer> integers = new ArrayList<>();
        for (int i = start; i < end; i++) {
            integers.add(i);
        }
        Collections.shuffle(integers, random);
        return integers;
    }

    /**
     * Replaces all dollar signs ($) with dots (.) in the input string. {@code Class.getName()} method uses the dollar
     * sign for separating inner classes from parent classes, but in code the separator has to be the dot.
     * @param s input string
     * @return string, with all dollar signs replaced with dots
     */
    public static String replaceDollars(String s) {
        return s.replaceAll("\\$", ".");
    }

    /**
     * Returns a raw type without generic information, corresponding to the specified general type
     * @param type type, for which the raw type is needed
     * @return raw type, without generic type information
     */
    public static Class toRawType(Type type) {
        try {
            if (type instanceof Class) {
                return (Class) type;
            } else if (type instanceof GenericArrayType) {
                GenericArrayType genericArrayType = (GenericArrayType) type;
                Type componentType = genericArrayType.getGenericComponentType();
                Class componentRawClass = toRawType(componentType);
                if (componentRawClass.isArray()) {
                    return Class.forName("[" + componentRawClass.getName());
                } else {
                    return Class.forName("[L" + componentRawClass.getName() + ";");
                }
            } else if (type instanceof ParameterizedType) {
                return toRawType(((ParameterizedType) type).getRawType());
            } else if (type instanceof TypeVariable) {
                TypeVariable typeVariable = (TypeVariable) type;
                return toRawType(typeVariable.getBounds()[0]);
            } else if (type instanceof WildcardType) {
                WildcardType wildcardType = (WildcardType) type;
                return toRawType(wildcardType.getUpperBounds()[0]);
            } else {
                throw new RuntimeException("Unknown subtype of " + Type.class.getName());
            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
