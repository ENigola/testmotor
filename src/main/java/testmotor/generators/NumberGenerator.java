package testmotor.generators;

import testmotor.GenerationException;
import testmotor.TestMotor;
import testmotor.tree.LeafNode;
import testmotor.tree.ValueNode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Generator for numeric literals. Generates values for types {@code byte}, {@code short}, {@code int}, {@code long},
 * {@code float} and {@code double}. Accepts primitive types' wrapper classes. Generated numbers will be of range
 * {@code minValue} (-120) to {@code maxValue} (360).
 */
public class NumberGenerator extends ValueGenerator {

	private int maxValue = 360;
	private int minValue = -120;

	private static Set<Class> types = new HashSet<>(Arrays.asList(
			byte.class, Byte.class,
			short.class, Short.class,
			int.class, Integer.class,
			long.class, Long.class,
			float.class, Float.class,
			double.class, Double.class
	));

	public NumberGenerator(TestMotor tm) {
		super(tm);
	}

	@Override
	public boolean canGenerate(Class type) {
		return types.contains(type);
	}

	@Override
	public ValueNode generate(Class type, int height) throws GenerationException {
		if (height != 0) {
			throw new GenerationException();
		}
		int totalRange = maxValue - minValue;
		Object value;
		if (type.equals((Byte.class)) || type.equals(Byte.TYPE)) {
			value = Integer.valueOf(tm.getRandom().nextInt(totalRange) + minValue).byteValue();
		} else if (type.equals((Short.class)) || type.equals(Short.TYPE)) {
			value = Integer.valueOf(tm.getRandom().nextInt(totalRange) + minValue).shortValue();
		} else if (type.equals((Integer.class)) || type.equals(Integer.TYPE)) {
			value = tm.getRandom().nextInt(totalRange) + minValue;
		} else if (type.equals((Long.class)) || type.equals(Long.TYPE)) {
			value = tm.getRandom().nextLong() % totalRange + minValue;
		} else if (type.equals((Float.class)) || type.equals(Float.TYPE)) {
			value = (float) (((int) ((tm.getRandom().nextFloat() * totalRange + minValue) * 1000)) / 1000.0);
		} else if (type.equals((Double.class)) || type.equals(Double.TYPE)) {
			value = (((int) ((tm.getRandom().nextDouble() * totalRange + minValue) * 1000)) / 1000.0);
		} else {
			throw new RuntimeException("Invalid type");
		}
		return new LeafNode(type, value);
	}
}
