package testmotor.generators;

import testmotor.GenerationException;
import testmotor.TestMotor;
import testmotor.tree.ValueNode;

import java.lang.reflect.Executable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Generator for arbitrary raw classes. Doesn't generate array types or primitive types, doesn't take into account
 * generic classes type parameters. Generates a method or constructor call. First {@link TestMotor}'s allowed executables
 * are checked. If none of those return a required type then the type itself and its subtypes are searched for constructors
 * and methods returning the required type.
 */
public class AnyClassGenerator extends ValueGenerator {

	public AnyClassGenerator(TestMotor tm) {
		super(tm);
	}

	@Override
	public boolean canGenerate(Class type) {
		return !type.isPrimitive() && !type.isArray();
	}

	@Override
	public ValueNode generate(Class type, int height) throws GenerationException {
		Set<Executable> usableExecutables = tm.getAllowedExecutablesForType(type);
		if (usableExecutables.size() == 0) {
			usableExecutables = tm.getSelfReturningExecutables(type);
		}
		List<Executable> candidateExecutables = new ArrayList<>(usableExecutables);
		while (candidateExecutables.size() != 0) {
			int i = tm.getRandom().nextInt(candidateExecutables.size());
			Executable exec = candidateExecutables.get(i);
			try {
				return tm.generateCall(exec, height);
			} catch (GenerationException e) {
				candidateExecutables.remove(i);
			}
		}
		throw new GenerationException(String.format("Can't generate value of type %s with height %d", type.getName(), height));
	}
}
