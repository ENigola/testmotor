package testmotor.generators;

import testmotor.GenerationException;
import testmotor.TestMotor;
import testmotor.tree.ExecNode;
import testmotor.tree.ValueNode;

import java.lang.reflect.*;
import java.util.*;

/**
 * Generator for Java collections. Generates an {@link Arrays#asList(Object[]) Arrays.asList} call, which will be wrapped
 * into a constructor of appropriate concrete type. The argument array generation for {@link Arrays#asList(Object[])}
 * is not done in this class. Can generate generic collections with specific type arguments.
 */
public class CollectionGenerator extends ValueGenerator {

    private static List<Class<? extends Collection>> generatableClasses = Arrays.asList(
            ArrayList.class, // List
            Vector.class, // List
            PriorityQueue.class, // Queue
            ArrayDeque.class, // Deque
            LinkedList.class, // List, Deque
            HashSet.class, // Set
            LinkedHashSet.class, // Set
            TreeSet.class // SortedSet
    );

    public CollectionGenerator(TestMotor tm) {
        super(tm);
    }

    @Override
    public boolean canGenerate(Class type) {
        return false;
    }

    @Override
    public boolean canGenerateGeneric(Type type) {
        Class classType = null;
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Type rawType = parameterizedType.getRawType();
            if (rawType instanceof Class) {
                classType = (Class) rawType;
            }
        } else if (type instanceof Class) {
            classType = (Class) type;
        }
        if (classType != null && Collection.class.isAssignableFrom(classType)) {
            for (Class<? extends Collection> generatableClass : generatableClasses) {
                if (classType.isAssignableFrom(generatableClass)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public ValueNode generate(Class type, int height) throws GenerationException {
        return generateGeneric(type, height);
    }

    @Override
    public ValueNode generateGeneric(Type type, int height) throws GenerationException {
        Class classType;
        Type componentType;
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            classType = (Class) parameterizedType.getRawType();
            componentType = parameterizedType.getActualTypeArguments()[0];
        } else if (type instanceof Class) {
            classType = (Class) type;
            componentType = Object.class;
        } else {
            throw new RuntimeException("Invalid type");
        }
        GenericArrayType arrayOfClassType = () -> componentType; // GenericArrayType::getGenericComponentType
        ValueNode arrayNode = tm.generateValue(arrayOfClassType, height);
        Method asListMethod;
        try {
            asListMethod = Arrays.class.getMethod("asList", Object[].class);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
        List<ValueNode> asListArgs = Collections.singletonList(arrayNode);
        ExecNode listNode = new ExecNode(asListMethod, null, asListArgs);
        Constructor collectionConstructor = null;
        try {
            for (Class<? extends Collection> collectionClass : generatableClasses) {
                if (classType.isAssignableFrom(collectionClass)) {
                    collectionConstructor = collectionClass.getConstructor(Collection.class);
                    break;
                }
            }
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
        List<ValueNode> collectionConstructorArgs = Collections.singletonList(listNode);
        return new ExecNode(collectionConstructor, null, collectionConstructorArgs);
    }
}
