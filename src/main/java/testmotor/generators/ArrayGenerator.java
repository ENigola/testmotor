package testmotor.generators;

import testmotor.GenerationException;
import testmotor.TestMotor;
import testmotor.Utils;
import testmotor.tree.ArrayNode;
import testmotor.tree.ValueNode;

import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static testmotor.Utils.toRawType;

/**
 * Generator for arrays. Can generate empty arrays. Can generate arrays of generic types.
 */
public class ArrayGenerator extends ValueGenerator {

	private int maxLength = 5;

	public ArrayGenerator(TestMotor tm) {
		super(tm);
	}

	@Override
	public boolean canGenerate(Class type) {
		return type.isArray();
	}

	@Override
	public boolean canGenerateGeneric(Type type) {
		return type instanceof GenericArrayType;
	}

	@Override
	public ValueNode generate(Class type, int height) throws GenerationException {
		return generateGeneric(type, height);
	}

	@Override
	public ValueNode generateGeneric(Type type, int height) throws GenerationException {
		Type componentType;
		if (type instanceof Class) {
			componentType = ((Class) type).getComponentType();
		} else if (type instanceof GenericArrayType) {
			componentType = ((GenericArrayType) type).getGenericComponentType();
		} else {
			throw new RuntimeException("Invalid type");
		}
		List<ValueNode> values = new ArrayList<>();
		if (height > 0) {
			int length = tm.getRandom().nextInt(maxLength) + 1; // 1 to 5 (inclusive)
			boolean requireHeight = true;
			for (int i = 0; i < length; i++) {
				if (requireHeight) {
					values.add(tm.generateValue(componentType, height - 1));
					requireHeight = false;
				} else {
					List<Integer> possibleHeights = Utils.shuffledRange(tm.getRandom(), 0, height);
					for (int j = 0; j < possibleHeights.size(); j++) {
						try {
							values.add(tm.generateValue(componentType, possibleHeights.get(j)));
							break;
						} catch (GenerationException e) {
							if (j == possibleHeights.size() - 1) {
								// Should not be reachable: should at lest be able to generate for height - 1,
								// 	if already reached this part
								throw new GenerationException();
							}
						}
					}
				}
			}
			Collections.shuffle(values, tm.getRandom()); // so that first element doesn't have to have max height
		}
		return new ArrayNode(toRawType(componentType), values);
	}
}
