package testmotor.generators;

import testmotor.GenerationException;
import testmotor.TestMotor;
import testmotor.Utils;
import testmotor.tree.LeafNode;
import testmotor.tree.ValueNode;

import java.lang.reflect.Executable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Generator for enums. Generates direct enum references or method calls from that enum class.
 */
public class EnumGenerator extends ValueGenerator {

	public EnumGenerator(TestMotor tm) {
		super(tm);
	}

	@Override
	public boolean canGenerate(Class type) {
		return type.isEnum();
	}

	@Override
	public ValueNode generate(Class type, int height) throws GenerationException {
		if (!Enum.class.isAssignableFrom(type)) {
			throw new IllegalArgumentException("Invalid type");
		}
		Set<Executable> allowedExecutables = tm.getAllowedExecutablesForType(type);
		boolean allowedExecutablesEmpty = allowedExecutables.isEmpty();
		List<Executable> candidateExecutables;
		if (allowedExecutablesEmpty) {
			Set<Executable> selfReturningExecutables = tm.getSelfReturningExecutables(type);
			candidateExecutables = new ArrayList<>(selfReturningExecutables);
		} else {
			candidateExecutables = new ArrayList<>(allowedExecutables);
		}
		try {
			// remove enum's valueOf method, because it usually throws an exception
			Method typeValueOf = type.getMethod("valueOf", String.class);
			candidateExecutables.remove(typeValueOf);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
		if (height == 0 && allowedExecutablesEmpty) {
			List<Executable> candidateZeroArgExecutables = new ArrayList<>();
			for (Executable executable : candidateExecutables) {
				if (executable.getParameterCount() == 0 && !Utils.isInstanceMethod(executable)) {
					candidateZeroArgExecutables.add(executable);
				}
			}
			if (!candidateZeroArgExecutables.isEmpty() && tm.getRandom().nextDouble() < 0.3) {
				int idx = tm.getRandom().nextInt(candidateZeroArgExecutables.size());
				return tm.generateCall(candidateZeroArgExecutables.get(idx), 0);
			} else {
				Object[] enumConstants = type.getEnumConstants();
				int idx = tm.getRandom().nextInt(enumConstants.length);
				return new LeafNode(((Enum) enumConstants[idx]).getDeclaringClass(), enumConstants[idx]);
			}
		} else {
			while (candidateExecutables.size() != 0) {
				int i = tm.getRandom().nextInt(candidateExecutables.size());
				Executable executable = candidateExecutables.get(i);
				try {
					return tm.generateCall(executable, height);
				} catch (GenerationException e) {
					candidateExecutables.remove(i);
				}
			}
		}
		throw new GenerationException(String.format("Can't generate value of type %s with height %d", type.getName(), height));
	}
}
