package testmotor.generators;

import testmotor.GenerationException;
import testmotor.TestMotor;
import testmotor.tree.ValueNode;

/**
 * Generator for values of type {@code Object}. Meant for generation value of the most general type {@code Object}.
 * Actually just generates values of type {@code String} and {@code StringBuilder}. This class exists for optimization
 * purposes only, as {@link AnyClassGenerator} can also generate values of type {@code Object}, but it would take a much
 * longer time to select a type because all classes would be included in the search space.
 */
public class ObjectGenerator extends ValueGenerator {

    public ObjectGenerator(TestMotor tm) {
        super(tm);
    }

    @Override
    public boolean canGenerate(Class type) {
        return type.equals(Object.class);
    }

    @Override
    public ValueNode generate(Class type, int height) throws GenerationException {
        if (height == 0) {
            return tm.generateValue(String.class, height);
        } else {
            return tm.generateValue(StringBuilder.class, height);
        }
    }
}
