package testmotor.generators;

import testmotor.GenerationException;
import testmotor.TestMotor;
import testmotor.tree.LeafNode;
import testmotor.tree.ValueNode;

/**
 * Generator for chars. Generates chars with ASCII table index 48 to 122 (include lower and upper case letters, numbers
 * and some punctuation marks).
 */
public class CharGenerator extends ValueGenerator {

	public CharGenerator(TestMotor tm) {
		super(tm);
	}

	@Override
	public boolean canGenerate(Class type) {
		return type.equals(char.class) || type.equals(Character.class);
	}

	@Override
	public ValueNode generate(Class type, int height) throws GenerationException {
		if (height != 0) {
			throw new GenerationException();
		}
		Character value = (char) (tm.getRandom().nextInt(75) + 48);
		return new LeafNode(type, value);
	}
}
