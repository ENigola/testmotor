package testmotor.generators;

import testmotor.GenerationException;
import testmotor.TestMotor;
import testmotor.tree.LeafNode;
import testmotor.tree.ValueNode;

/**
 * Generator for strings. Only generates string literals. Generated strings can be empty. Generated strings only include
 * numbers and ASCII lower and upper case letters.
 */
public class StringGenerator extends ValueGenerator {

	private int maxLength = 8;

	public StringGenerator(TestMotor tm) {
		super(tm);
	}

	@Override
	public boolean canGenerate(Class type) {
		return type.equals(String.class);
	}

	@Override
	public ValueNode generate(Class type, int height) throws GenerationException {
		if (height != 0) {
			throw new GenerationException();
		}
		StringBuilder sb = new StringBuilder();
		int length;
		if (tm.getRandom().nextDouble() < 0.4) {
			length = 1;
		} else {
			length = tm.getRandom().nextInt(maxLength + 1);
		}
		for (int i = 0; i < length; i++) {
			char c;
			double d = tm.getRandom().nextDouble();
			if (d < 0.6) {
				c = (char) (tm.getRandom().nextInt(26) + 'a');
			} else if (d < 0.8) {
				c = (char) (tm.getRandom().nextInt(26) + 'A');
			} else {
				c = (char) (tm.getRandom().nextInt(10) + '0');
			}
			sb.append(c);
		}
		return new LeafNode(String.class, sb.toString());
	}
}
