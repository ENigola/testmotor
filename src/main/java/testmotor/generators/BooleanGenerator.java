package testmotor.generators;

import testmotor.GenerationException;
import testmotor.TestMotor;
import testmotor.tree.LeafNode;
import testmotor.tree.ValueNode;

/**
 * Generator for boolean values. Selects randomly between the two possible.
 */
public class BooleanGenerator extends ValueGenerator {

	public BooleanGenerator(TestMotor tm) {
		super(tm);
	}

	@Override
	public boolean canGenerate(Class type) {
		return type.equals(boolean.class) || type.equals(Boolean.class);
	}

	@Override
	public ValueNode generate(Class type, int height) throws GenerationException {
		if (height != 0) {
			throw new GenerationException();
		}
		Boolean value = tm.getRandom().nextBoolean();
		return new LeafNode(type, value);
	}
}
