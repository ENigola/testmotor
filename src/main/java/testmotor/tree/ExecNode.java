package testmotor.tree;

import testmotor.Utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Executable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static testmotor.Utils.*;

/**
 * A node representing a method or constructor call.
 */
public class ExecNode extends ValueNode{
	private Executable executable;
	private ValueNode parentInstance;
	private List<ValueNode> arguments;

	/**
	 * @param executable method or constructor of the call that this object represents
	 * @param parentInstance object instance, on which the method will be called; for constructors and class (static)
	 *                       methods this argument is ignored and can be null
	 * @param arguments	list of arguments for the executable (method or constructor)
	 * @throws IllegalArgumentException if specified executable is an instance method and the parent instance is null
	 */
	public ExecNode(Executable executable, ValueNode parentInstance, List<ValueNode> arguments) {
		if (isInstanceMethod(executable) && parentInstance == null) {
			throw new IllegalArgumentException("Argument parentInstance can't be null for instance methods");
		}
		this.executable = executable;
		this.parentInstance = parentInstance;
		this.arguments = arguments;
	}

	@Override
	public String stringify() {
		StringBuilder call = new StringBuilder();
		if (isConstructor(executable)) {
			call.append("new ");
		} else if (isInstanceMethod(executable)) {
			call.append(parentInstance.stringify()).append(".");
		} else {
			call.append(Utils.replaceDollars(executable.getDeclaringClass().getName())).append(".");
		}
		call.append(Utils.replaceDollars(executable.getName()));
		call.append("(");
		List<ValueNode> callArguments = new ArrayList<>(arguments);
		// unwrap final argument (array) if executable is vararg
		if (executable.isVarArgs()) {
			ArrayNode varArg = (ArrayNode) callArguments.remove(callArguments.size() - 1);
			callArguments.addAll(varArg.getValues());
		}
		for (int i = 0; i < callArguments.size(); i++) {
			if (i != 0) {
				call.append(", ");
			}
			call.append(callArguments.get(i).stringify());
		}
		call.append(")");
		return call.toString();
	}

	@Override
	public Object invoke() throws InvocationTargetException {
		Object[] argumentValues = new Object[arguments.size()];
		for (int i = 0; i < arguments.size(); i++) {
			argumentValues[i] = arguments.get(i).invoke();
		}
		try {
			executable.setAccessible(true);
			if (isConstructor(executable)) {
				return ((Constructor) executable).newInstance(argumentValues);
			} else if (isInstanceMethod(executable)) {
				return ((Method) executable).invoke(parentInstance.invoke(), argumentValues);
			} else {
				return ((Method) executable).invoke(null, argumentValues);
			}
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
}
