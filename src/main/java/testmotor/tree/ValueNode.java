package testmotor.tree;

import java.lang.reflect.InvocationTargetException;

/**
 * The abstract superclass of all the nodes that make up the in-memory tree representation of generated method or
 * constructor calls.
 */
public abstract class ValueNode {

	/**
	 * Invokes the expression represented by this node and its sub-nodes and returns the value.
	 * @return return value of the underlying expression
	 * @throws InvocationTargetException if the underlying executables throw an error
	 */
	public abstract Object invoke() throws InvocationTargetException;

	/**
	 * Return the string representation of the expression represented by this tree structure.
	 * Returned string will be a legal Java expression. All names (methods, classes) will be fully qualified.
	 * @return a legal Java expression, representing this tree structure
	 */
	public abstract String stringify();
}
