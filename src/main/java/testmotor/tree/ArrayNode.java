package testmotor.tree;

import testmotor.Utils;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * A node representing an array.
 */
public class ArrayNode extends ValueNode {
	private Class componentType;
	private List<ValueNode> values;

	/**
	 * @param componentType type of the array components
	 * @param values list of array elements (represented by {@code ValueNode} instances)
	 */
	public ArrayNode(Class componentType, List<ValueNode> values) {
		this.componentType = componentType;
		this.values = values;
	}

	@Override
	public Object invoke() throws InvocationTargetException {
		Object arrayObject = Array.newInstance(componentType, values.size());
		for (int i = 0; i < values.size(); i++) {
			Array.set(arrayObject, i, values.get(i).invoke());
		}
		return arrayObject;
	}

	@Override
	public String stringify() {
		int dimensions = getDimensions();
		StringBuilder arrayStr = new StringBuilder("new " + Utils.replaceDollars(getFinalComponentType().getName()));
		for (int i = 0; i < dimensions; i++) {
			arrayStr.append("[]");
		}
		arrayStr.append(" ");
		arrayStr.append(stringifyValues());
		return arrayStr.toString();
	}

	// Required for unwrapping varargs
	List<ValueNode> getValues() {
		return values;
	}

	private String stringifyValues() {
		StringBuilder initializerValues = new StringBuilder("{");
		for (int i = 0; i < values.size(); i++) {
			if (i != 0) {
				initializerValues.append(", ");
			}
			ValueNode valueNode = values.get(i);
			if (valueNode instanceof ArrayNode) {
				initializerValues.append(((ArrayNode) valueNode).stringifyValues());
			} else {
				initializerValues.append(values.get(i).stringify());
			}
		}
		initializerValues.append("}");
		return initializerValues.toString();
	}

	private int getDimensions() {
		int counter = 1;
		Class nextComponentType = componentType.getComponentType();
		while (nextComponentType != null) {
			counter++;
			nextComponentType = nextComponentType.getComponentType();
		}
		return counter;
	}

	private Class getFinalComponentType() {
		Class nextComponentType = componentType;
		while (nextComponentType.isArray()) {
			nextComponentType = nextComponentType.getComponentType();
		}
		return nextComponentType;
	}
}
