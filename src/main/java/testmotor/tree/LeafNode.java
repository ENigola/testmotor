package testmotor.tree;

import testmotor.Utils;

import java.util.Arrays;
import java.util.List;

/**
 * A node representing some final value. Could be a literal (primitive or string) value or enum reference.
 */
public class LeafNode extends ValueNode {
	private Class type;
	private Object value;

	private static List<Class> primitiveWrappers = Arrays.asList(
			Boolean.class,
			Byte.class,
			Character.class,
			Short.class,
			Integer.class,
			Long.class,
			Float.class,
			Double.class
	);

	/**
	 * @param type the type (class) of the value
	 * @param value the value that this node represents
	 * @throws IllegalArgumentException if the {@code type} class is not assignable from the runtime class of {@code value},
	 * except for primitive types and primitives' wrappers
	 */
	public LeafNode(Class type, Object value) {
		if (!type.isPrimitive() && !primitiveWrappers.contains(type) && !type.isAssignableFrom(value.getClass())) {
			throw new IllegalArgumentException(String.format("Given type %s is not assignable from value type %s", type.getName(), value.getClass().getName()));
		}
		this.type = type;
		this.value = value;
	}

	@Override
	public Object invoke() {
		return value;
	}

	@Override
	public String stringify() {
		if (type.isPrimitive() || primitiveWrappers.contains(type)) {
			if (type.equals(Boolean.class) || type.equals(boolean.class)) {
				return "" + value;
			} else if (type.equals((Byte.class)) || type.equals(byte.class)) {
				return "(byte) " + value;
			} else if (type.equals((Character.class)) || type.equals(char.class)) {
				return "'" + value + "'";
			} else if (type.equals((Short.class)) || type.equals(short.class)) {
				return "(short) " + value;
			} else if (type.equals((Integer.class)) || type.equals(int.class)) {
				return "" + value;
			} else if (type.equals((Long.class)) || type.equals(long.class)) {
				return "" + value + "L";
			} else if (type.equals((Float.class)) || type.equals(float.class)) {
				return "" + value + "f";
			} else if (type.equals((Double.class)) || type.equals(double.class)) {
				return "" + value;
			} else {
				throw new RuntimeException("Unknown primitive type");
			}
		} else if (type.equals(String.class)) {
			return "\"" +  value + "\"";
		} else if (Enum.class.isAssignableFrom(type)) {
			Enum enumValue = (Enum) value;
			return Utils.replaceDollars(type.getName()) + "." + enumValue.name();
		} else {
			throw new RuntimeException("Cannot stringify type " + type.getName());
		}
	}
}
