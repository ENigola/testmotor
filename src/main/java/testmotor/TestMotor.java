package testmotor;

import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfo;
import io.github.classgraph.ClassInfoList;
import io.github.classgraph.ScanResult;
import testmotor.generators.*;
import testmotor.tree.ExecNode;
import testmotor.tree.ValueNode;

import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.*;
import java.util.*;

import static testmotor.Utils.*;

/**
 * The central class for generation test data. This class contains method for generation test data and it also coordinates
 * that process. To generate test data, first an instance of this class needs to be created: {@link TestMotor#TestMotor()}.
 * After that {@link TestMotor#generateTestData(Executable, int, Set, boolean)} or
 * {@link TestMotor#generateTestDataCombinatorial(Executable, List, List, int, boolean)} can be used to generate
 * test data.
 */
public class TestMotor {
	private ScanResult typesScanResult;
	private Random random;
	private List<ValueGenerator> generators;
	private int noExceptionGenerationTries = 30;
	private Set<Executable> allowedExecutables = Collections.emptySet();

	private static List<Class<? extends ValueGenerator>> defaultGenerators = Arrays.asList(
			ObjectGenerator.class,
			NumberGenerator.class,
			BooleanGenerator.class,
			CharGenerator.class,
			StringGenerator.class,
			ArrayGenerator.class,
			EnumGenerator.class,
			CollectionGenerator.class,
			AnyClassGenerator.class
	);

	/**
	 * New TestMotor instance using only default value generators.
	 */
	public TestMotor() {
		this(new ArrayList<>());
	}

	/**
	 * New TestMotor instance with custom value generators added. Takes a list of classes as argument, instances will be
	 * created automatically.
	 * @param customGenerators list of custom generator classes
	 */
	public TestMotor(List<Class<? extends ValueGenerator>> customGenerators) {
		this.typesScanResult = new ClassGraph().enableClassInfo().enableSystemJarsAndModules().scan();
		this.random = new Random();
		this.generators = instantiateGenerators(customGenerators);
	}

	/**
	 * Generates test data combinatorially in batches. For each combination of specified heights and allowed executables
	 * {@code amountPerCombination} test data instances are generated. All generated test data for a single set of allowed
	 * executables is put into one list, regardless of used heights. Finally a list those lists is returned.
	 * @param executable executables, for which test data needs to be generated
	 * @param heights list of heights for generated test data
	 * @param allowedExecutablesList list of sets of allowed executables, which should be used for each different batch of
	 *                               generated test data
	 * @param amountPerCombination amount of test data instances to generated for each height and executables combination
	 * @param allowExceptions whether exception throwing test data is allowed or not
	 * @return generated test data; one inner list represents one set of allowed executables
	 */
	public List<List<ValueNode>> generateTestDataCombinatorial(Executable executable,
															   List<Integer> heights,
															   List<Set<Executable>> allowedExecutablesList,
															   int amountPerCombination,
															   boolean allowExceptions) {
		List<List<ValueNode>> batches = new ArrayList<>();
		for (Set<Executable> allowedExecutables : allowedExecutablesList) {
			List<ValueNode> batch = new ArrayList<>();
			for (Integer height : heights) {
				try {
					for (int i = 0; i < amountPerCombination; i++) {
						batch.add(generateTestData(executable, height, allowedExecutables, allowExceptions));
					}
				} catch (GenerationException e) {
					// continue with next height
				}
			}
			batches.add(batch);
		}
		return batches;
	}

	/**
	 * Generates a single test call of an executable. This is the method that kicks of the test data generation process.
	 * @param executable executable for which the test data needs to be generated
	 * @param height height of the generated test data, i.e. executable call, tree representation
	 * @param allowedExecutables set of executables that should be used in the generation process, rather than other
	 *                           executables with the same return type; can be null
	 * @param allowExceptions if false, then generated test data will be checked for exceptions and regenerated if any
	 *                        are thrown
	 * @return generated test data
	 * @throws GenerationException if generation fails
	 */
	public ValueNode generateTestData(Executable executable, int height, Set<Executable> allowedExecutables, boolean allowExceptions) throws GenerationException {
		setAllowedExecutables(allowedExecutables);
		ValueNode result = generateCall(executable, height);
		if (!allowExceptions) {
			// Disable System.out while checking for exceptions
			PrintStream originalOut = System.out;
			PrintStream dummyOut = new PrintStream(new OutputStream() {
				@Override
				public void write(int b) {}
			});
			System.setOut(dummyOut);
			try {
				boolean validTestDataGenerated = false;
				for (int i = 0; i < noExceptionGenerationTries; i++) {
					try {
						result.invoke();
						validTestDataGenerated = true;
						break;
					} catch (InvocationTargetException e) {
						result = generateCall(executable, height);
					}
				}
				if (!validTestDataGenerated) {
					throw new GenerationException("Could not generate call for executable " + executable + " with height "
							+ height + " without exceptions in " + noExceptionGenerationTries + " tries.");
				}
			} finally {
				setAllowedExecutables(null);
				System.setOut(originalOut);
			}
		}
		setAllowedExecutables(null);
		return result;
	}

	/**
	 * Sets the seed of random number generator
	 * @param seed seed for random number generator
	 */
	public void setSeed(int seed) {
		this.random.setSeed(seed);
	}

	/**
	 * Gets {@link Random} instance associated with this class.
	 * @return {@link Random} instance
	 */
	public Random getRandom() {
		return random;
	}

	/**
	 * Sets the number of times, that TestMotor will try to generate test data that doesn't throw an exception. Default
	 * is 30.
	 * @param tries number of tries
	 */
	public void setNoExceptionGenerationTries(int tries) {
		this.noExceptionGenerationTries = tries;
	}

	/**
	 * Sets the allowed executables (methods and constructors) that should be used for generating values of necessary type.
	 * @param allowedExecutables set of allowed executables
	 */
	private void setAllowedExecutables(Set<Executable> allowedExecutables) {
		if (allowedExecutables == null) {
			this.allowedExecutables = Collections.emptySet();
		} else {
			this.allowedExecutables = allowedExecutables;
		}
	}

	/**
	 * Retrieves the subset of allowed executables, which return a value of specified type (including its subtypes).
	 * @param returnType return type of returned executables
	 * @return set of executables, that return a value of specified type or its subtype.
	 */
	public Set<Executable> getAllowedExecutablesForType(Class returnType) {
		Set<Executable> allowedExecutablesForType = new HashSet<>();
		for (Executable executable : allowedExecutables) {
			if (isMethod(executable) && returnType.isAssignableFrom(((Method) executable).getReturnType())) {
				allowedExecutablesForType.add(executable);
			} else if (isConstructor(executable) && returnType.isAssignableFrom(((Constructor) executable).getDeclaringClass())) {
				allowedExecutablesForType.add(executable);
			}
		}
		return allowedExecutablesForType;
	}

	/**
	 * Creates instances of the provided custom generators and default generators and returns those instances in a list.
	 * @param customGenerators list of custom generators to instantiate in addition to default generators
	 * @return list of generator instances; first the custom generators in the same order as provided and then default
	 * generators
	 */
	private List<ValueGenerator> instantiateGenerators(List<Class<? extends ValueGenerator>> customGenerators) {
		List<Class<? extends ValueGenerator>> generatorClasses = new ArrayList<>(customGenerators);
		generatorClasses.addAll(defaultGenerators);
		List<Constructor<? extends ValueGenerator>> generatorConstructors = new ArrayList<>();
		for (Class<? extends ValueGenerator> generatorClass : generatorClasses) {
			try {
				generatorConstructors.add(generatorClass.getConstructor(TestMotor.class));
			} catch (NoSuchMethodException e) {
				throw new IllegalArgumentException("Class " + generatorClass.getName()
						+ " does not have a public constructor that takes TestMotor instance as argument");
			}
		}
		List<ValueGenerator> generators = new ArrayList<>();
		for (Constructor<? extends ValueGenerator> constructor : generatorConstructors) {
			try {
				generators.add(constructor.newInstance(this));
			} catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
				throw new RuntimeException("Instantiating generator " + constructor.getDeclaringClass().getName() + " failed", e);
			}
		}
		return generators;
	}

	/**
	 * Generates a method or constructor call. The call tree will be of required height, but not necessarily balanced or
	 * symmetric in any way.
	 * @param executable executable for which the call needs to be generated
	 * @param height required height of the generated tree representation of the call
	 * @return generated call tree
	 * @throws GenerationException if generation fails
	 */
	public ValueNode generateCall(Executable executable, int height) throws GenerationException {
		if (height < 0) {
			throw new GenerationException("Min height reached");
		}
		ValueNode parentInstance = null;
		ValueNode[] arguments = new ValueNode[executable.getParameterCount()];
		// marks if a value with required height is already generated
		// only one tree branch (parent instance or arguments) is required to be of height h-1 (allows uneven trees)
		boolean requireHeight = true;
		// order in which arguments are generated, -1 denotes parent class instance (needed for instance methods)
		List<Integer> argumentIndexes = Utils.shuffledRange(random, -1, executable.getParameterCount());
		for (int i : argumentIndexes) {
			ValueNode value = null;
			Type type;
			if (i == -1) {
				if (isInstanceMethod(executable)) {
					type = executable.getDeclaringClass();
				} else {
					continue; // no value needed for parent instance
				}
			} else {
				type = executable.getGenericParameterTypes()[i];
			}
			// possible heights for value generation; if can't generate with one, next one is picked
			List<Integer> possibleHeights = Utils.shuffledRange(random, 0, height);
			if (requireHeight) {
				try {
					value = generateValue(type, height - 1);
					requireHeight = false;
				} catch (GenerationException e) {
					possibleHeights.remove(Integer.valueOf(height - 1));
				}
			}
			if (value == null) {
				while (!possibleHeights.isEmpty()) {
					try {
						int argumentHeight = possibleHeights.remove(0);
						value = generateValue(type, argumentHeight);
						break;
					} catch (GenerationException e) {
						// continue with next height
					}
				}
				if (value == null) {
					throw new GenerationException("Can't generate value for type " + type.getTypeName());
				}
			}
			if (i == -1) {
				parentInstance = value;
			} else {
				arguments[i] = value;
			}
		}
		if (requireHeight && height != 0) {
			throw new GenerationException("Can't generate call for executable " + executable);
		}
		return new ExecNode(executable, parentInstance, Arrays.asList(arguments));
	}

	/**
	 * Generates an expression which will return a value of specified type. The expression could be for example a method
	 * call or a literal. Generation process itself is delegated to a generator, which can generate a value of that type.
	 * @param targetType return type of the generated expression
	 * @param height require height of the generated tree structure
	 * @return generated value (expression in tree format)
	 * @throws GenerationException if generation fails or if a generator for the specified type is not found
	 */
	public ValueNode generateValue(Type targetType, int height) throws GenerationException {
		if (height < 0) {
			throw new GenerationException("Min height reached");
		}
		for (ValueGenerator generator : generators) {
			if (generator.canGenerateGeneric(targetType)) {
				return generator.generateGeneric(targetType, height);
			} else {
				Class rawType = Utils.toRawType(targetType);
				if (generator.canGenerate(rawType)) {
					return generator.generate(rawType, height);
				}
			}
		}
		throw new GenerationException("None of the used generators can generate value of type " + targetType);
	}

	/**
	 * Finds all public constructors and public methods, which return a value of type {@code targetClass} or its subtype,
	 * from {@code targetClass} and its subclasses (or subinterfaces).
	 * @param targetClass type for which to find executables
	 * @return set of executables returning a value of type {@code targetClass} or its subtype.
	 */
	public Set<Executable> getSelfReturningExecutables(Class targetClass) {
		if (!Modifier.isPublic(targetClass.getModifiers())) {
			// ignore non public classes
			return Collections.emptySet();
		}
		Set<Executable> executables = new HashSet<>();
		// add constructors
		if (!Modifier.isAbstract(targetClass.getModifiers())) {
			executables.addAll(Arrays.asList(targetClass.getConstructors()));
		}
		// add methods which return targetClass instance
		Method[] methods = targetClass.getMethods();
		for (Method m : methods) {
			if (targetClass.isAssignableFrom(m.getReturnType())) {
				executables.add(m);
			}
		}
		// add subclasses' constructors and methods recursively
		if (!Modifier.isFinal(targetClass.getModifiers())) {
			ClassInfo classInfo = typesScanResult.getClassInfo(targetClass.getName());
			ClassInfoList classInfoList;
			if (classInfo.isInterface()) {
				classInfoList = classInfo.getClassesImplementing();
			} else {
				classInfoList = classInfo.getSubclasses();
			}
			for (ClassInfo subType : classInfoList) {
				executables.addAll(getSelfReturningExecutables(subType.loadClass()));
			}
		}
		return executables;
	}
}
